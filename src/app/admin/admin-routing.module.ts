import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AccountComponent } from './pages/account/account.component';
import { PlaybookComponent } from './pages/playbook/playbook.component';
import { PlaysrunningComponent } from './pages/playsrunning/playsrunning.component';
import { AccountListComponent } from './pages/account/account-list/account-list.component';
import { ListsSpecComponent } from './pages/account/lists-spec/lists-spec.component';
import { ContactComponent } from './pages/account/contact/contact.component';
import { ContactListComponent } from './pages/account/contact-list/contact-list.component';
import { ContaComponent } from './pages/account/conta/conta.component';
import { IntegrationsComponent } from './pages/integrations/integrations.component';
import { ListsAddComponent } from './pages/account/lists-add/lists-add.component';
import { AccountItemComponent } from './pages/account/account/account.component';
import { AccountAddComponent } from './pages/account/account-add/account-add.component';
import { ContactAddComponent } from './pages/account/contact-add/contact-add.component';
import { ContactListAddComponent } from './pages/account/contact-list-add/contact-list-add.component';
import { FirstEntryComponent } from './pages/first-entry/first-entry.component';
import { NewPlayComponent } from './pages/new-play/new-play.component';
import { TargetComponent } from './pages/new-play/target/target.component';
import { GoalComponent } from './pages/new-play/goal/goal.component';
import { TargetAccountComponent } from './pages/new-play/target-account/target-account.component';
import { TargetPeopleComponent } from './pages/new-play/target-people/target-people.component';
import { ChooseComponent } from './pages/new-play/choose/choose.component';


const routes: Routes = [
  { path: '', component: AdminComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'first-entry' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'account', component: AccountComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'list' },
          { path: 'account-list', component: AccountListComponent },
          { path: 'accounts', component: AccountItemComponent },
          { path: 'account-add', component: AccountAddComponent },
          { path: 'lists-add', component: ListsAddComponent },
          { path: 'contact-list-add', component: ContactListAddComponent },
          { path: 'list-spec', component: ListsSpecComponent },
          { path: 'contacts', component: ContactComponent },
          { path: 'contact-add', component: ContactAddComponent },
          { path: 'contact-list', component: ContactListComponent },
          { path: 'conta', component: ContaComponent }
        ]
      },
      { path: 'playbook', component: PlaybookComponent },
      { path: 'playsrunning/:id', component: PlaysrunningComponent },
      { path: 'playsrunning/', component: PlaysrunningComponent },
      { path: 'integrations', component: IntegrationsComponent },
      { path: 'first-entry', component: FirstEntryComponent },
      { path: 'newplay', component: NewPlayComponent,
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'choose-template' },
          { path: 'choose-template', component: ChooseComponent },
          { path: 'goal', component: GoalComponent },
          { path: 'target', component: TargetComponent,
            children: [
              { path: '', pathMatch: 'full', redirectTo: 'account-list' },
              { path: 'account-list', component: TargetAccountComponent },
              { path: 'people-list', component: TargetPeopleComponent }
            ]
           },   
        ]
      },
    ]
 },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
