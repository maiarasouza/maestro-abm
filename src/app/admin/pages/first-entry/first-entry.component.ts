import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Company } from 'src/app/model/company';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CompanyService } from 'src/app/services/company.service';
import Swal from 'sweetalert2';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-first-entry',
  templateUrl: './first-entry.component.html',
  styleUrls: ['./first-entry.component.scss']
})
export class FirstEntryComponent implements OnInit {
  public hasCompany: boolean = false;
  public username: string;
  public name: string;
  public surname: string;
  addCompanyForm: FormGroup;
  company: Company;
  
  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private companyService: CompanyService,
    private authenticationService: AuthenticationService) {

      
    if (localStorage.getItem('token') == null) {
      this.router.navigate(['/auth/login']);
    }
    this.username = window.localStorage.getItem('username');
    this.name = window.localStorage.getItem('name');
    this.surname = window.localStorage.getItem('surname');

    if(window.localStorage.getItem('company_id') != "null" && window.localStorage.getItem('company_id') != null){
      this.hasCompany = true;
      this.loadCompany();
    }

    this.addCompanyForm = this.formBuilder.group({
      name: [''],
      ein: [''],      
      n_of_employees: [''],//int
      revenue: [''],//int
      industry: [''],
      description: [''],
      geolocation: ['']
    });
     }

  ngOnInit() {
    
  }

  registerCompany() {  
    this.companyService.registerCompany(this.addCompanyForm.value)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            if(window.localStorage.getItem('company_id') != "null" && window.localStorage.getItem('company_id') != null){
              this.hasCompany = true;
              this.loadCompany();
            }
            this.router.navigate(['/admin/first-entry'], { replaceUrl: true });
          });          
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  updateCompany() {  
    this.companyService.updateCompany(this.addCompanyForm.value)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            this.router.navigate(['/admin/first-entry'], { replaceUrl: true });
          });   
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })          
        });
  }

  loadCompany() {  
    this.companyService.getCompanyById()
      .pipe(first())
      .subscribe(
        data => {
          this.company = data;
          this.addCompanyForm.patchValue(this.company);
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })          
        });
  }

}
