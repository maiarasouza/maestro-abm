import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-integrations',
  templateUrl: './integrations.component.html',
  styleUrls: ['./integrations.component.scss']
})
export class IntegrationsComponent implements OnInit {
  public integration;
  public integrations: any[] = [
    {
      id: 0,
      company: './assets/logos/analytics.png',
      description: 'Vá para o trbalho mais rápido e confortável.'
    },
    {
      id: 1,
      company: './assets/logos/hubspot.png',
      description: 'Vá para o trbalho mais rápido e confortável.'
    },
    {
      id: 2,
      company: './assets/logos/pipedrive.png',
      description: 'Vá para o trbalho mais rápido e confortável.'
    },
    {
      id: 3,
      company: './assets/logos/calender.png',
      description: 'Vá para o trbalho mais rápido e confortável.'
    },
    {
      id: 4,
      company: './assets/logos/LinkedinAds.png',
      description: 'Vá para o trbalho mais rápido e confortável.'
    },
  ];
  
  public integrationID: FormGroup = this.fb.group({
    id: ['', Validators.required]
  });

  constructor(public helper: HelperService, private fb: FormBuilder) { }

  ngOnInit() {
  }

  public openIntegration(type?) {
      this.integration =! this.integration;
  }

}
