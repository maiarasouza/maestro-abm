import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';
import { HelperService } from 'src/app/services/helper.service';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Contact } from 'src/app/model/contact';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss', '../account.component.scss']
})
export class ContactComponent implements OnInit {

  contacts: any[];
  contact: Contact;
  contactDetailForm: FormGroup = this.formBuilder.group({
    name: [''],
    description: [''],
    geolocation: [''],
    email: [''],
    phone: [''],
    website: [''],
    linkedin: [''],
    engagement: [''],
    page_views: [''],
    accounts: [{account_id: '', account_name: ''}],
    role: [''],
    plays: ['']
  });

  constructor(
    public helper: HelperService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService
  ) {
    this.loadContacts();
    this.initializeContactDetailForm();



  }

  ngOnInit() { }

  loadContacts() {
    this.contactService.getContactsByCompany()
      .pipe(first())
      .subscribe(
        data => {
          this.contacts = data;
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  async detailContactItem(contact: Contact) {
    this.initializeContactDetailForm();
    await this.contactService.getContactById(contact._id)
      .pipe(first())
      .subscribe(
        data => {
          this.contact = data;
          this.contactDetailForm.patchValue(this.contact);
          // console.log('data', data)
          // console.log("dentro",typeof this.contact.accounts[0].account_name)
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  updateContact() {
    this.contactService.updateContactById(this.contact._id, this.contactDetailForm.value)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            this.loadContacts();
          });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  deleteContact() {
    Swal.fire({
      title: 'Do you want exclude account?',
      showCancelButton: true,
      confirmButtonText: `Exclude`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.contactService.deleteContactById(this.contact._id)
          .pipe(first())
          .subscribe(
            data => {
              Swal.fire(
                'Good job!',
                'Successfuly Excluded!',
                'success'
              ).then((result) => {
                this.loadContacts();
              });
            },
            error => {
              Swal.fire({
                title: 'Oops...',
                text: 'Something went wrong!',
                icon: 'error',
                confirmButtonText: 'Close'
              })
            });
      }
    })
  }

  initializeContactDetailForm() {
    this.contactDetailForm.setValue({
      name: '',
      description: '',
      geolocation: '',
      email: '',
      phone: '',
      website: '',
      linkedin: '',
      engagement: '',
      page_views: '',
      accounts: [{account_id: '', account_name: ''}],
      role: '',
      plays: ''
    });
  }

  search(c: string) {
    c = c ? c.trim() : '';
    this.contacts = this.contacts.filter(fa => fa.name.toLowerCase().includes(c.toLowerCase()));
    console.log(c, this.contacts);
  }

}