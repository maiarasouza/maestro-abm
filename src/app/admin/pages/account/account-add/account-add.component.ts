import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AccountListService } from 'src/app/services/account-list.service';
import { AccountService } from 'src/app/services/account.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-account-add',
  templateUrl: './account-add.component.html',
  styleUrls: ['./account-add.component.scss']
})
export class AccountAddComponent implements OnInit {

  accountAddForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService
  ) {
    this.accountAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],      
      geolocation: ['', Validators.required],//int
      industry: ['', Validators.required],
      n_of_employees: ['', Validators.required],
      website: ['', Validators.required],
      revenue: ['', Validators.required],
      engagement: ['', Validators.required],
      page_views: ['', Validators.required]
    });

  }

  get f() { return this.accountAddForm.controls; }

  ngOnInit() {
  }

  addAccountList() {   
    this.loading = true;
    this.accountService.addAccount(this.accountAddForm.value)
      .pipe(first())
      .subscribe(
        data => {         
          this.router.navigate(['/admin/account/accounts'], { replaceUrl: true });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Invalid Username/Password!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }



}
