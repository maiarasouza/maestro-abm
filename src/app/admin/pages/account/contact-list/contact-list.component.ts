import { Component, OnInit } from "@angular/core";
import { ContactListService } from "src/app/services/contact-list.service";
import { HelperService } from "src/app/services/helper.service";
import { first } from "rxjs/operators";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ContactList } from "src/app/model/contactList";
import Swal from "sweetalert2";
import { ActivatedRoute, Router } from "@angular/router";
import { ContactService } from "src/app/services/contact.service";
import { pipe } from "rxjs";

@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.component.html",
  styleUrls: ["./contact-list.component.scss", "../account.component.scss"],
})
export class ContactListComponent implements OnInit {
  contacts: any[];
  contactItems: any[];
  contactList: ContactList;
  contacts_not_in_contactList: any[];
  listContactForm: FormGroup = this.formBuilder.group({
    _id: [""],
    name: [""],
    company_id: [""],
    number_contacts: [""],
    type: [""],
    created_by: [""],
    created_since: [""],
    updated_in: [""],
    favorite: [""],
    contact: [""],
  });

  constructor(
    public helper: HelperService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private contactListService: ContactListService,
    private contactService: ContactService
  ) {
    this.loadContacts();
    this.loadContactLists();
    this.initializeListContactForm();
  }

  ngOnInit() {}

  loadContacts() {
    this.contactService
      .getContactsByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.contactItems = data;
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  loadContactLists() {
    this.contactListService
      .getContactListByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.contacts = data;
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  async detailContactList(account: ContactList) {
    this.initializeListContactForm();
    await this.contactListService
      .getAccountListById(account._id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.contactList = data;
          this.listContactForm.patchValue(this.contactList);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  updateContactList() {
    if (
      this.listContactForm.get("contact").value != "null" &&
      this.listContactForm.get("contact").value != ""
    ) {
      var result = this.contactItems.filter((obj) => {
        return obj._id === this.listContactForm.get("contact").value;
      });

      this.contactService
        .addContactInContactList(
          this.listContactForm.get("_id").value,
          result[0]
        )
        .subscribe(
          (data) => {
            this.contactListService
              .updateContactListById(this.listContactForm.value)
              .subscribe(
                (data) => {
                  Swal.fire("Good job!", "Successfuly Update!", "success").then(
                    (result) => {
                      this.loadContactLists();
                    }
                  );
                },
                (error) => {
                  Swal.fire({
                    title: "Oops...",
                    text: "Something went wrong!",
                    icon: "error",
                    confirmButtonText: "Close",
                  });
                }
              );
          },
          (error) => {
            Swal.fire({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              confirmButtonText: "Close",
            });
          }
        );
    } else {
      this.contactListService
        .updateContactListById(this.listContactForm.value)
        .pipe(first())
        .subscribe(
          (data) => {
            Swal.fire("Good job!", "Successfuly Update!", "success").then(
              (result) => {
                this.loadContactLists();
              }
            );
          },
          (error) => {
            Swal.fire({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              confirmButtonText: "Close",
            });
          }
        );
    }
  }

  deleteContactList() {
    Swal.fire({
      title: "Do you want exclude account list?",
      showCancelButton: true,
      confirmButtonText: `Exclude`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.contactListService
          .deleteContactListById(this.contactList._id)
          .pipe(first())
          .subscribe(
            (data) => {
              Swal.fire("Good job!", "Successfuly Excluded!", "success").then(
                (result) => {
                  this.loadContactLists();
                }
              );
            },
            (error) => {
              Swal.fire({
                title: "Oops...",
                text: "Something went wrong!",
                icon: "error",
                confirmButtonText: "Close",
              });
            }
          );
      }
    });
  }

  contactNotInContactList() {
    this.contacts_not_in_contactList = [];
    this.contacts_not_in_contactList = this.contactItems.filter(
      (n) => !this.contactList.contacts.some((o) => o.contact_id == n._id)
    );
    console.log("result", this.contacts_not_in_contactList);
  }

  initializeListContactForm() {
    this.listContactForm.setValue({
      _id: "",
      name: "",
      company_id: "",
      number_contacts: "",
      type: "",
      created_by: "",
      created_since: "",
      updated_in: "",
      favorite: "",
      contact: "",
    });
    this.contacts_not_in_contactList = [];
  }

  search(c: string) {
    c = c ? c.trim() : "";
    this.contacts = this.contacts.filter((fa) =>
      fa.name.toLowerCase().includes(c.toLowerCase())
    );
    console.log(c, this.contacts);
  }
}
