import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AccountListService } from 'src/app/services/account-list.service';

@Component({
  selector: 'app-lists-add',
  templateUrl: './lists-add.component.html',
  styleUrls: ['./lists-add.component.scss']
})
export class ListsAddComponent implements OnInit {

  listsAddForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountListService: AccountListService
  ) {
    this.listsAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      number_accounts: ['', Validators.required],      
      type: ['', Validators.required],//int
      favorite: [false, Validators.required]
    });

  }

  get f() { return this.listsAddForm.controls; }

  ngOnInit() {
  }

  addAccountList() {
    
    // stop here if form is invalid
   // if (this.listsAddForm.invalid) {
   //   return;
   // }

    this.loading = true;
    this.accountListService.addAccountList(this.listsAddForm.value)
      .pipe(first())
      .subscribe(
        data => {         
          this.router.navigate(['/admin/account/account-list'], { replaceUrl: true });
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }



}
