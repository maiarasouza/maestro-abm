import { Component, OnInit } from "@angular/core";
import { AccountListService } from "src/app/services/account-list.service";
import { HelperService } from "src/app/services/helper.service";
import { first } from "rxjs/operators";
import { AccountEntityItem } from "src/app/model/accountEntityItem";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AccountList } from "src/app/model/accountList";
import Swal from "sweetalert2";
import { ActivatedRoute, Router } from "@angular/router";
import { AccountService } from "src/app/services/account.service";
import { AccountItem } from "src/app/model/accountItem";
import { connectableObservableDescriptor } from "rxjs/internal/observable/ConnectableObservable";

@Component({
  selector: "app-lists",
  templateUrl: "./account-list.component.html",
  styleUrls: ["./account-list.component.scss", "../account.component.scss"],
})
export class AccountListComponent implements OnInit {
  account: any[];
  accounts: AccountList[];
  accountList: AccountList;
  accountItems: AccountEntityItem[];
  accounts_not_in_accountList: any[];
  listsDetailForm: FormGroup = this.formBuilder.group({
    _id: [""],
    name: [""],
    company_id: [""],
    number_accounts: [""],
    type: [""],
    created_by: [""],
    created_since: [""],
    updated_in: [""],
    favorite: [""],
    accountItem: [""],
  });

  constructor(
    public helper: HelperService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountListService: AccountListService,
    private accountService: AccountService
  ) {
    this.loadAccounts();
    this.loadAccountLists();
    this.initializeListsDetailForm();
  }

  ngOnInit() {}

  loadAccounts() {
    this.accountService
      .getAccountByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.accountItems = data;
          console.log('accountItems', this.accountItems)
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  loadAccountLists() {
    this.accountListService
      .getAccountListByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.accounts = data;
          console.log('accounts', this.accounts);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  async detailRegister(account: AccountList) {
    this.initializeListsDetailForm() 
    await this.accountListService
      .getAccountListById(account._id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.accountList = data;
          this.listsDetailForm.patchValue(this.accountList);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  updateAccountList() {
    if (
      this.listsDetailForm.get("accountItem").value != "null" &&
      this.listsDetailForm.get("accountItem").value != ""
    ) {
      var result = this.accountItems.filter((obj) => {
        return obj._id === this.listsDetailForm.get("accountItem").value;
      });

      this.accountService
        .addAccountInList(this.listsDetailForm.get("_id").value, result[0])
        .subscribe((data) => {
          this.accountListService
            .updatetAccountListById(this.listsDetailForm.value)
            .subscribe(
              (data) => {
                Swal.fire("Good job!", "Successfuly Update!", "success").then(
                  (result) => {
                    this.loadAccountLists();
                  }
                );
              },
              (error) => {
                Swal.fire({
                  title: "Oops...",
                  text: "Something went wrong!",
                  icon: "error",
                  confirmButtonText: "Close",
                });
              }
            );
        });
    } else {
      this.accountListService
        .updatetAccountListById(this.listsDetailForm.value)
        .subscribe(
          (data) => {
            Swal.fire("Good job!", "Successfuly Update!", "success").then(
              (result) => {
                this.loadAccountLists();
              }
            );
          },
          (error) => {
            Swal.fire({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              confirmButtonText: "Close",
            });
          }
        );
    }
  }

  deleteAccountList() {
    Swal.fire({
      title: "Do you want exclude account list?",
      showCancelButton: true,
      confirmButtonText: `Exclude`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.accountListService
          .deleteAccountListById(this.accountList._id)
          .pipe(first())
          .subscribe(
            (data) => {
              Swal.fire("Good job!", "Successfuly Update!", "success").then(
                (result) => {
                  this.loadAccountLists();
                }
              );
            },
            (error) => {
              Swal.fire({
                title: "Oops...",
                text: "Something went wrong!",
                icon: "error",
                confirmButtonText: "Close",
              });
            }
          );
      }
    });
  }
  accountsNotInAccountList() {
    this.accounts_not_in_accountList = [];
    this.accounts_not_in_accountList = this.accountItems.filter(
      (n) => !this.accountList.accounts.some((o) => o.account_id == n._id)
    );
    console.log("result", this.accounts_not_in_accountList);
  }

  initializeListsDetailForm() {
    this.listsDetailForm.setValue({
      _id: "",
      name: "",
      company_id: "",
      number_accounts: "",
      type: "",
      created_by: "",
      created_since: "",
      updated_in: "",
      favorite: "",
      accountItem: "",
    });
    this.accounts_not_in_accountList = [];
  }

  search(s: string) {
    s = s ? s.trim() : "";
    console.log(s);
    this.accounts = this.accounts.filter((fa) =>
      fa.name.toLowerCase().includes(s.toLowerCase())
    );
  }
}
