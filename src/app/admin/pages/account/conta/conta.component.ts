import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-conta',
  templateUrl: './conta.component.html',
  styleUrls: ['./conta.component.scss', '../account.component.scss']
})
export class ContaComponent implements OnInit {

  public base = true;
  public activeP = false;
  public tasks = false;
  public details = false;

  conta: any[] = [ 
    {
      id: 1,
      name: 'Person s Name',
      role: 'CEO',
      email: 'contato@gmail.com',
      phone: '(11)0000-0000',
      linkedin: 'www.linkedin.com/name'
    },
    {
      id: 2,
      name: 'Person Name',
      role: 'Head de Mkt',
      email: 'contato@gmail.com',
      phone: '(11)0000-0000',
      linkedin: 'www.linkedin.com/name'
    },
    {
      id: 3,
      name: 'Person Name',
      role: 'Head de Mkt',
      email: 'contato@gmail.com',
      phone: '(11)0000-0000',
      linkedin: 'www.linkedin.com/name'
    },
  ];

  plays: any[] = [ 
    {
      id: 1,
      type:'Pré-Play',
      status: 'on',
      percent: '75',
      start: '08/11/2020',
      end: '08/11/2020'
    },
    {
      id: 2,
      type:'Prospect Play',
      status: 'on',
      percent: '75',
      start: '08/11/2020',
      end: '08/11/2020'
    },
    {
      id: 3,
      type:'Event Play',
      status: 'finished',
      percent: '100',
      start: '08/11/2020',
      end: '08/11/2020'
    },
    {
      id: 4,
      type:'CS Play',
      status: 'on',
      percent: '75',
      start: '08/11/2020',
      end: '08/11/2020'
    },
    {
      id: 5,
      type:'Renewal Play',
      status: 'draft',
      percent: '75',
      start: '08/11/2020',
      end: '08/11/2020'
    },
    {
      id: 6,
      type:'Churn Prevetion Play',
      status: 'on',
      percent: '75',
      start: '08/11/2020',
      end: '08/11/2020'
    }
  ];

  mytasks: any[] = [ 
    {
      id: 1,
      name:'Setup campaign',
      status: 'open',
      burndown: '15',
      deadline: '08/11/2020',
      assigned: 'Lucy',
    },
    {
      id: 2,
      name:'Create Prospect Play for December',
      status: 'closed',
      burndown: '42',
      deadline: '08/11/2020',
      assigned: 'Lucy',
    },
    {
      id: 3,
      status: 'open',
      burndown: '10',
      name:'Setup campaign',
      deadline: '08/11/2020',
      assigned: 'Lucy',
    }
  ];
  constructor( public helper: HelperService ) { }

  ngOnInit() { }

  public contas(type) {
     switch (type) {
      case 'base':
        this.base = true;
        this.activeP = false;         
        this.tasks = false;         
        this.details = false; 
        break;

     case 'active':
        this.activeP = true;  
        this.base = false;        
        this.tasks = false;         
        this.details = false;         
        break;

      case 'tasks':
        this.tasks = true;     
        this.base = false;
        this.activeP = false;        
        this.details = false;      
        break;

      case 'details':
        this.details = true;  
        this.base = false;
        this.activeP = false;         
        this.tasks = false;          
        break;

      default:
        break;
     }
  }
}
