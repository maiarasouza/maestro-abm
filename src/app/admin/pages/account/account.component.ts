import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  
  constructor( public helper: HelperService) { }

  ngOnInit() { }

  public openList(type) {
    const a = document.getElementById('accounts') as HTMLImageElement;
    const al = document.getElementById('account-list') as HTMLImageElement;
    const c = document.getElementById('contacts') as HTMLImageElement;
    const cl = document.getElementById('contact-list') as HTMLImageElement;

    switch (type) {
      case 'accounts':
        a.src = './assets/icons/building.svg';
        al.src = './assets/icons/blue-building.svg';
        c.src = './assets/icons/user-blue.svg';
        cl.src = './assets/icons/group.svg';        
        break;
      case 'account-list':
        a.src = './assets/icons/blue-building-a.svg';
        al.src = './assets/icons/buildings.svg';
        c.src = './assets/icons/user-blue.svg';
        cl.src = './assets/icons/group.svg';        
        break;
      case 'contacts':
        a.src = './assets/icons/blue-building-a.svg';
        al.src = './assets/icons/blue-building.svg';
        c.src = './assets/icons/user-orange-c.svg';
        cl.src = './assets/icons/group.svg';        
        break;
      case 'contact-list':
        a.src = './assets/icons/blue-building-a.svg';
        al.src = './assets/icons/blue-building.svg';
        c.src = './assets/icons/user-blue.svg';
        cl.src = './assets/icons/orange-group.svg';        
        break;
    
      default:
        break;
    }
  }  

}
