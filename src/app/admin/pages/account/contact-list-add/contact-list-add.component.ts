import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ContactListService } from 'src/app/services/contact-list.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact-list-add',
  templateUrl: './contact-list-add.component.html',
  styleUrls: ['./contact-list-add.component.scss']
})
export class ContactListAddComponent implements OnInit {

  listsAddForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private contactListService: ContactListService
  ) {
    this.listsAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      number_accounts: ['', Validators.required],      
      type: ['', Validators.required],//int
      favorite: [false, Validators.required]
    });

  }

  get f() { return this.listsAddForm.controls; }

  ngOnInit() {
  }

  addContactList() {
    
    this.contactListService.addContactList(this.listsAddForm.value)
      .pipe(first())
      .subscribe(
        data => {         
          this.router.navigate(['/admin/account/contact-list'], { replaceUrl: true });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }



}
