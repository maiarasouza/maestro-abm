import { Component, OnInit } from "@angular/core";
import { ContactService } from "src/app/services/contact.service";
import { HelperService } from "src/app/services/helper.service";
import { first } from "rxjs/operators";
import { AccountService } from "src/app/services/account.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { AccountEntityItem } from "src/app/model/accountEntityItem";
import Swal from "sweetalert2";
import { ActivatedRoute, Router } from "@angular/router";
import Popper from "popper.js";

@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss", "../account.component.scss"],
})
export class AccountItemComponent implements OnInit {
  accounts: any[];
  show: boolean = false;
  account: AccountEntityItem;
  contacts: any[];
  contacts_not_in_account: any[];
  accountDetailForm: FormGroup = this.formBuilder.group({
    _id: [""],
    name: [""],
    company_id: [""],
    description: [""],
    geolocation: [""], //int
    industry: [""],
    n_of_employees: [""],
    website: [""],
    revenue: [""],
    engagement: [""],
    page_views: [""],
    contact: [""],
  });

  constructor(
    public helper: HelperService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService,
    private contactService: ContactService
  ) {
    this.loadContacts();
    this.loadAccounts();
    this.initializeAccountDetailForm();
  }

  ngOnInit() {}

  loadContacts() {
    this.contactService
      .getContactsByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.contacts = data;
          console.log("contacts by company", data);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  loadAccounts() {
    this.accountService
      .getAccountByCompany()
      .pipe(first())
      .subscribe(
        (data) => {
          this.accounts = data;
          console.log("accounts by company", data);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  async detailAccount(account: AccountEntityItem) {
    this.initializeAccountDetailForm();
    await this.accountService
      .getAccountById(account._id)
      .pipe(first())
      .subscribe(
        (data) => {
          this.account = data;
          this.accountDetailForm.patchValue(this.account);
        },
        (error) => {
          Swal.fire({
            title: "Oops...",
            text: "Something went wrong!",
            icon: "error",
            confirmButtonText: "Close",
          });
        }
      );
  }

  updateAccount() {
    if (
      this.accountDetailForm.get("contact").value != "null" &&
      this.accountDetailForm.get("contact").value != ""
    ) {
      var result = this.contacts.filter((obj) => {
        return obj._id === this.accountDetailForm.get("contact").value;
      });

      this.accountService
        .addContactInList(this.accountDetailForm.get("_id").value, result[0])
        .subscribe(
          (data) => {
            this.accountService
              .updatetAccountById(this.accountDetailForm.value)
              .subscribe(
                (data) => {
                  Swal.fire("Good job!", "Successfuly Update!", "success").then(
                    (result) => {
                      this.loadAccounts();
                    }
                  );
                },
                (error) => {
                  Swal.fire({
                    title: "Oops...",
                    text: "Something went wrong!",
                    icon: "error",
                    confirmButtonText: "Close",
                  });
                }
              );
          },
          (error) => {
            Swal.fire({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              confirmButtonText: "Close",
            });
          }
        );
    } else {
      this.accountService
        .updatetAccountById(this.accountDetailForm.value)
        .pipe(first())
        .subscribe(
          (data) => {
            Swal.fire("Good job!", "Successfuly Update!", "success").then(
              (result) => {
                this.loadAccounts();
              }
            );
          },
          (error) => {
            Swal.fire({
              title: "Oops...",
              text: "Something went wrong!",
              icon: "error",
              confirmButtonText: "Close",
            });
          }
        );
    }
  }

  deleteAccount() {
    Swal.fire({
      title: "Do you want exclude account?",
      showCancelButton: true,
      confirmButtonText: `Exclude`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.accountService
          .deleteAccountById(this.account._id)
          .pipe(first())
          .subscribe(
            (data) => {
              Swal.fire("Good job!", "Successfuly Excluded!", "success").then(
                (result) => {
                  this.loadAccounts();
                }
              );
            },
            (error) => {
              Swal.fire({
                title: "Oops...",
                text: "Something went wrong!",
                icon: "error",
                confirmButtonText: "Close",
              });
            }
          );
      }
    });
  }

  contactsNotInAccount() {
    this.contacts_not_in_account = [];
    this.contacts_not_in_account = this.contacts.filter(
      (n) => !this.account.contacts.some((o) => o.contact_id == n._id)
    );
    console.log("result", this.contacts_not_in_account);
  }

  initializeAccountDetailForm() {
    this.accountDetailForm.setValue({
      _id: "",
      name: "",
      company_id: "",
      description: "",
      geolocation: "", //int
      industry: "",
      n_of_employees: "",
      website: "",
      revenue: "",
      engagement: "",
      page_views: "",
      contact: "",
    });
    this.contacts_not_in_account = [];
  }

  search(a: string) {
    a = a ? a.trim() : "";
    console.log(a);
    this.accounts = this.accounts.filter((fa) =>
      fa.name.toLowerCase().includes(a.toLowerCase())
    );
  }
}
