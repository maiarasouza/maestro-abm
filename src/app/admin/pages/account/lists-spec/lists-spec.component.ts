import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-lists-spec',
  templateUrl: './lists-spec.component.html',
  styleUrls: ['./lists-spec.component.scss', '../account.component.scss']
})
export class ListsSpecComponent implements OnInit {

  listSpec: any[] = [ 
    {
      id: 1,
      name:'Worc',
      website: 'www.website.com',
      influence: '14',
      engagement: '64%',
      min: '153min',
      page: '7'
    },
    {
      id: 2,
      name:'Distrito',
      website: 'www.website.com',
      influence: '14',
      engagement: '64%',
      min: '153min',
      page: '7'
    },
    {
      id: 3,
      name:'Deploy',
      website: 'www.website.com',
      influence: '14',
      engagement: '64%',
      min: '153min',
      page: '7'
    },
  ];

  constructor( public helper: HelperService) { }

  ngOnInit() { }

}
