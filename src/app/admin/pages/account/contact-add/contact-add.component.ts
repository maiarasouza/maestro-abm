import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AccountListService } from 'src/app/services/account-list.service';
import { AccountService } from 'src/app/services/account.service';
import { ContactService } from 'src/app/services/contact.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent implements OnInit {

  contactAddForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService
  ) {
    this.contactAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],      
      geolocation: ['', Validators.required],//int
      email: ['', Validators.required],
      phone: ['', Validators.required],
      website: ['', Validators.required],
      linkedin: ['', Validators.required],
      engagement: ['', Validators.required],
      page_views: ['', Validators.required],
      role:['']
    });

  }

  get f() { return this.contactAddForm.controls; }

  ngOnInit() {
  }

  addAccountList() {   

    this.loading = true;
    this.contactService.addContact(this.contactAddForm.value)
      .pipe(first())
      .subscribe(
        data => {         
          this.router.navigate(['/admin/account/contacts'], { replaceUrl: true });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Invalid Username/Password!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }



}
