import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayService } from '../../../services/play.service'
import { PlaybookTemplates } from '../../../model/playbookTemplates'

@Component({
  selector: 'app-playbook',
  templateUrl: './playbook.component.html',
  styleUrls: ['./playbook.component.scss'],
  providers: [PlaybookTemplates]
})
export class PlaybookComponent implements OnInit {
  public myplays: any[]
  public play;
  public createawareness = false;
  public createpreplay = false;
  public createmkt = false;
  public createsales = false;
  public createcs = false;

  constructor(
    private _route: Router,
    public playService: PlayService,
    public readonly playbookTemplates: PlaybookTemplates,
  ) { }

  ngOnInit() {
    this.play = this.playbookTemplates.preplay;
    this.playService.getAllPlay().subscribe(result => { this.myplays = result })
  }
  getSubType(e) {
    localStorage.setItem('subType', e);
    console.log(e)
  }

  public playbook(type) {
    switch (type) {
      case 'Myplays':
        this.play = this.myplays;
        break;

      case 'Awareness':
        this.play = this.playbookTemplates.awareness;
        break;

      case 'Preplay':
        this.play = this.playbookTemplates.preplay;
        break;

      case 'Marketing':
        this.play = this.playbookTemplates.marketing;
        break;

      case 'Sales':
        this.play = this.playbookTemplates.sales;
        break;

      case 'CS':
        this.play = this.playbookTemplates.cs;
        break;
      default:
        break;
    }
  }

  public openCreate(type) {
    switch (type) {

      case 'a':
        this.createawareness = !this.createawareness;
        localStorage.setItem('Type', 'Awareness');
        break;

      case 'p':
        this.createpreplay = !this.createpreplay;
        localStorage.setItem('Type', 'Preplay');
        break;

      case 'm':
        this.createmkt = !this.createmkt;
        localStorage.setItem('Type', 'Marketing');
        break;

      case 's':
        this.createsales = !this.createsales;
        localStorage.setItem('Type', 'Sales');
        break;

      case 'c':
        this.createcs = !this.createcs;
        localStorage.setItem('Type', 'CS');
        break;

      default:
        break;
    }
  }
}
