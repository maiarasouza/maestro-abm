import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CompanyService } from 'src/app/services/company.service';
import { first } from 'rxjs/operators';
import { Company } from 'src/app/model/company';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public hasCompany: boolean = false;
  addCompanyForm: FormGroup;
  company: Company;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private companyService: CompanyService,
    private authenticationService: AuthenticationService) { 

    if (localStorage.getItem('token') == null) {
      this.router.navigate(['/auth/login']);
    }

    if(window.localStorage.getItem('company_id') != "null"){
      this.hasCompany = true
    }

    this.addCompanyForm = this.formBuilder.group({
      name: [''],
      ein: [''],      
      n_of_employees: [''],//int
      revenue: [''],//int
      industry: [''],
      description: [''],
      geolocation: ['']
    });
  }

  ngOnInit() {
  }

  registerCompany() {  
    this.companyService.registerCompany(this.addCompanyForm.value)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            this.router.navigate(['/admin/dashboard'], { replaceUrl: true });
          });          
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  updateCompany() {  
    this.companyService.updateCompany(this.addCompanyForm.value)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            this.router.navigate(['/admin/dashboard'], { replaceUrl: true });
          });   
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })          
        });
  }

  loadCompany() {  
    this.companyService.getCompanyById()
      .pipe(first())
      .subscribe(
        data => {
          this.company = data;
          this.addCompanyForm.patchValue(this.company);
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })          
        });
  }


}
