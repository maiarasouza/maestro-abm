import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSlideToggleChange } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { HelperService } from "src/app/services/helper.service";
import { PlayService } from "../../../services/play.service";
import { AccountService } from "../../../services/account.service";
import Swal from "sweetalert2";
import { first } from "rxjs/operators";
import { TouchpointActions } from "../../../model/touchpointActions"
import { TouchpointModel } from "../../../model/touchpoint.interface"
import { TouchpointService } from "src/app/services/touchpoint.service";
import { ContactService } from "../../../services/contact.service";
import { CompanyService } from "src/app/services/company.service";
import { DatePipe } from '@angular/common';

@Component({
  selector: "app-playsrunning",
  templateUrl: "./playsrunning.component.html",
  styleUrls: ["./playsrunning.component.scss"],
  providers: [
    TouchpointActions,
  ],
})
export class PlaysrunningComponent implements OnInit {
  touchpoint: [{_id: string, touchpoint_id: string, touchpoint_name: string, created_since: Date, start_date: Date, end_date: Date, channel: string, status: boolean}]
  play: any;
  label: string = "..."
  id: string
  countTouch: number
  start_date: string
  end_date: string
  expected_end_date: string
  team: boolean = true;
  team_companies: boolean = false;
  time_to_close: boolean = false;
  successful_touchpoints: boolean = false;
  scorecard: boolean = false;
  positions: boolean = false;
  cause: boolean = false;
  opportunity: boolean = false;
  saved: boolean = false;
  worked: boolean = false;
  responders: boolean = false;

  constructor(private formBuilder: FormBuilder,
    public helper: HelperService,
    public route: ActivatedRoute,
    public router: Router,
    public playService: PlayService,
    public accountService: AccountService,
    public contactService: ContactService,
    public companyService: CompanyService,
    public touchpointActions: TouchpointActions,
    public touchpointService: TouchpointService,) { }

  // -------------------------------------------------------------------------

  // READ PLAY

  ngOnInit() {


    this.id = this.route.snapshot.paramMap.get("id");
    this.playService.getPlayById(this.id).subscribe((dados) => {
      this.play = dados
      this.start_date = this.play.start_date == this.play.start_date ? new Date(this.play.start_date).toLocaleDateString("pt-BR") : "Not set"
      this.end_date = this.play.end_date == this.play.end_date ? new Date(this.play.end_date).toLocaleDateString('pt-BR') : "Not set"
      this.expected_end_date = this.play.expected_end_date == this.play.expected_end_date ? new Date(this.play.expected_end_date).toLocaleDateString("pt-BR") : "Not set"
      this.touchpoint = dados.touchpoints
      console.log(this.play)
      console.table(this.touchpoint)
      const status = document.getElementById("going") as HTMLImageElement;
      console.log("status", status)
      if (this.play.status) {
        this.label = "Ongoing";
      } else {
        this.label = "Closed";
        status.style.color = "red";
        status.style.backgroundColor = "#ff39394d";
      }
    });
  }

  // -------------------------------------------------------------------------

  // UPDATE PLAY

  Playdata = new Object();

  getGoal(goal) {
    this.Playdata['goal'] = goal
    console.log("goal", this.Playdata)
  }
  getTarget(target) {
    this.Playdata['target'] = parseInt(target)
    console.log("target", this.Playdata)
  }
  getBudget(budget) {
    this.Playdata['budget'] = parseInt(budget)
    console.log("budget", this.Playdata)
  }
  getStartDate(startDate) {
    this.Playdata['start_date'] = new Date(startDate)
    console.log("start_date", this.Playdata);
    // const start_date = new DatePipe('en-US').transform(this.Playdata['start_date'], 'dd/MM/yyyy')
    // console.log(start_date);
  }
  getEndDate(endDate) {
    this.Playdata['expected_end_date'] = new Date(endDate)
    console.log("expected_end_date", this.Playdata)
  }
  getNamePlay(name) {
    this.Playdata['name'] = name
    console.log("play name", this.Playdata)
  }

  updatePlay() {
    this.playService.updatePlayById(this.play._id, this.Playdata)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigateByUrl('/', { skipLocationChange: true })
            .then(() => this.router.navigate(['/admin/playsrunning/', data.id], { replaceUrl: true }))
          Swal.fire(
            'Great!',
            'Play successfuly updated!',
            'success'
          )
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'No cake for you!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  // -------------------------------------------------------------------------

  // DELETE PLAY

  deletePlay() {
    Swal.fire({
      title: 'Do you want exclude play?',
      showCancelButton: true,
      confirmButtonText: `Exclude`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.playService.deletePlayById(this.play._id)
          .pipe()
          .subscribe(
            data => {

              Swal.fire(
                'Good job!',
                'Successfuly Excluded!',
                'success'
              ).then((result) => {
                this.router.navigate(['/admin/playbook'])
              });
            },
            error => {
              Swal.fire({
                title: 'Oops...',
                text: 'Something went wrong!',
                icon: 'error',
                confirmButtonText: 'Close'
              })
            });
      }
    })
  }

  // -------------------------------------------------------------------------

  // END PLAY

  form: FormGroup = this.formBuilder.group({
    team: ['', Validators.required],
    team_companies: ['', Validators.required],
    time_to_close: ['', Validators.required],
    successful_touchpoints: ['', Validators.required],
    scorecard: ['', Validators.required],
    positions: ['', Validators.required],
    cause: ['', Validators.required],
    opportunity: [false, Validators.required],
    file: ['']
  });

  initializeEndPlay() {
    this.form.setValue({
      team: '',
      team_companies: '',
      time_to_close: '',
      successful_touchpoints: '',
      scorecard: '',
      positions: '',
      cause: '',
      opportunity: '',
      file: '',
    })
    this.successful_touchpoints = false;
    this.time_to_close = false;
    this.team = false;
    this.team_companies = false;
    this.scorecard = false;
    this.positions = false;
    this.cause = false;
    this.opportunity = false;
    this.saved = true;
  }

  next(number?) {
    switch (number) {
      case 'team':
        if (this.form.controls.team.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.team_companies = true;
          this.team = false;
          console.log(this.form.controls.team.value);
        }
        break;

      case 'team_companies':
        if (this.form.controls.team_companies.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.team_companies = false;
          this.team = false;
          this.time_to_close = true;
          console.log(this.form.controls.team_companies.value);
        }
        break;

      case 'time_to_close':
        if (this.form.controls.time_to_close.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.successful_touchpoints = true;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          console.log(this.form.controls.time_to_close.value);
        }
        break;

      case 'successful_touchpoints':
        if (this.form.controls.successful_touchpoints.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.successful_touchpoints = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = true
          console.log(this.form.controls.successful_touchpoints.value);
        }
        break;

      case 'scorecard':
        if (this.form.controls.scorecard.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.successful_touchpoints = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = false;
          this.positions = true;
          console.log(this.form.controls.scorecard.value);
        }
        break;
      case 'positions':
        if (this.form.controls.positions.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.successful_touchpoints = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = false;
          this.positions = false;
          this.cause = true;
          console.log(this.form.controls.positions.value);
        }
        break;

      case 'cause':
        if (this.form.controls.cause.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.successful_touchpoints = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = false;
          this.positions = false;
          this.cause = false;
          this.opportunity = true;
          console.log(this.form.controls.cause.value);
        }
        break;
      default:
        break;
    }
  }

  back(number?) {
    switch (number) {
      case 'team_companies':
        this.team = true;
        this.team_companies = false;
        this.time_to_close = false;
        break;

      case 'time_to_close':
        this.team = false;
        this.team_companies = true;
        this.opportunity = false;
        this.time_to_close = false;
        break;

      case 'successful_touchpoints':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = true;
        this.opportunity = false;
        this.successful_touchpoints = false;
        break;

      case 'scorecard':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.successful_touchpoints = true;
        this.opportunity = false;
        this.scorecard = false;
        break;

      case 'positions':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.successful_touchpoints = false;
        this.scorecard = true;
        this.opportunity = false;
        this.positions = false;
        break;

      case 'cause':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.successful_touchpoints = false;
        this.scorecard = false;
        this.positions = true;
        this.cause = false;
        this.opportunity = false;
        break;

      case 'opportunity':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.successful_touchpoints = false;
        this.scorecard = false;
        this.positions = false;
        this.cause = true;
        this.opportunity = false;
        break;

      default:
        break;
    }
  }

  endPlay() {

    console.log(this.form.value);
    if (this.form.controls.opportunity.invalid === true) {
      alert('Preencha o campo corretamente')
    } else {
      console.log(this.form.controls.opportunity.value);
      this.playService.endPlayById(this.play._id, this.form.value)
        .pipe()
        .subscribe(
          data => {
            console.log(data)
            Swal.fire(
              'Good job!',
              'Successfuly Ended!',
              'success'
            )
            // .then(() => this.router.navigate(['/admin/playsrunning/', this.play._id], { replaceUrl: true }))
          },
          error => {
            Swal.fire({
              title: 'Oops...',
              text: 'Something went wrong!',
              icon: 'error',
              confirmButtonText: 'Close'
            })
          });
      this.initializeEndPlay()
    }
  }

  backToPlay() {
    console.log("funcao back to play"),
    this.router.navigateByUrl('/', { skipLocationChange: true })
    .then((() => this.router.navigate(['/admin/playsrunning/', this.play._id], { replaceUrl: true })))
    .then(() => this.initializeEndPlay())
  }

  // -------------------------------------------------------------------------

  // PROGRESS BAR

  percent = false;
  advance = 0;
  totalTouchpointsForPlay = 5;

  progressBar() {
    this.percent = !this.percent;
    this.advance = (this.play.touchpoints.length / this.totalTouchpointsForPlay) * 100;
    console.log("advace", this.advance)
    const img = document.getElementById("open") as HTMLImageElement;
    if (this.percent === true) {
      img.src = "./assets/icons/btn-up.svg";
    } else {
      img.src = "./assets/icons/btn-down.svg";
    }
  }

  // -------------------------------------------------------------------------

  // READ TOUCHPOINT

  toggle: boolean;
  current_touchpoint: TouchpointModel
  is_late: boolean;
  getName(touchpoint_id) {
    console.log("getName touchpoint_id_current", touchpoint_id)
    this.playService.getUniqueTouchbyId(touchpoint_id)
      .subscribe(dados => {
        this.current_touchpoint = dados,
        console.log("current touch", this.current_touchpoint)
          this.toggle = !this.current_touchpoint.status,
          (new Date(dados.start_date).getTime() > new Date(dados.end_date).getTime() ? this.is_late=true : this.is_late=false)
      })
  }

  getTogle(value: MatSlideToggleChange) {
    console.log("getTogle")
    const { checked } = value;
    // let color = 'accent';
    // this.current_touchpoint.status = checked;
    // let disabled = false;
    // console.log(checked, color, disabled)
  }


  // -------------------------------------------------------------------------

  // CREATE TOUCHPOINT
  touchpointModel: TouchpointModel;
  openwink: boolean;
  openpoke: boolean;
  openstalk: boolean;
  openflirt: boolean;
  showT: boolean;
  for_who: any[];
  contacts: any[];
  responsibles: any[];
  event: string;
  action: string;

  form_touchpoint: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    play_id: ['', Validators.required],
    start_date: [new Date(), Validators.required],
    end_date: [new Date(), Validators.required],
    behaviour: ['', Validators.required],
    for_who: [[]],
    description: [''],
    responsibles: [[]],
    triggers: [[]],
    icebreaker: [[]],
    action: [[]],
    attachments: [[]],
    contacts: [[]],
  })

  initializeTouchpoint() {
    this.form_touchpoint.setValue({
      name: '',
      play_id: this.play._id,
      start_date: new Date(),
      end_date: new Date(),
      behaviour: '',
      for_who: this.play.accounts.map(({ account_id, account_name }) => ({ account_id, account_name })),
      description: '',
      responsibles: [],
      triggers: [],
      icebreaker: [],
      action: [],
      attachments: [],
      contacts: []
    })
    this.openwink = false;
    this.openpoke = false;
    this.openstalk = false;
    this.openflirt = false;
    this.showT = false;
    this.for_who = [];
    this.contacts = [];
    this.responsibles = [];
    this.event = '';
    this.action = '';
    this.getResponsiblesByCompany();
    this.getContactsByAccount();
  }

  getAction(event) {
    localStorage.setItem('Action', event);
    console.log('Teste:', event);
    this.event = event
  }

  openAction(action) {
    switch (action) {
      case 'Wink':
        this.openwink = !this.openwink;
        localStorage.setItem('Action', 'Wink');
        this.action = action;
        break;

      case 'Poke':
        this.openpoke = !this.openpoke;
        localStorage.setItem('Action', 'Poke');
        this.action = action;
        break;

      case 'Stalk':
        this.openstalk = !this.openstalk;
        localStorage.setItem('Action', 'Stalk');
        this.action = action;
        break;

      case 'Flirt':
        this.openflirt = !this.openflirt;
        localStorage.setItem('Action', 'Flirt');
        this.action = action;
        break;

      default:
        break;
    }
  }

  getResponsiblesByCompany() {
    this.companyService.getCompanyById()
      .subscribe(response => {
        for (let i = 0; i < response.username.length; i++) {
          console.log(response.username[i])
          this.responsibles.push({ 'responsible': response.username[i] })
        }
      })
    console.log(this.responsibles)
  }
  getContactsByAccount() {
    this.accountService.getAccountsByPlay(this.play._id)
      .subscribe(response => {
        for (let i = 0; i < response.length; i++) {
          for (let j = 0; j < response[i].contacts.length; j++) {
            this.contacts.push({ 'contact_id': response[i].contacts[j].contact_id, 'contact_name': response[i].contacts[j].contact_name })


          }
        }
        // this.contacts.map(({ contact_id, contact_name }) => ({ contact_id, contact_name }))
        console.log("contacts", this.contacts)
      })
  }

  createTouchpoint() {
    this.form_touchpoint.value.behaviour = this.action;
    this.form_touchpoint.value.action = this.event;
    console.log("no create", this.form_touchpoint.value)
    this.touchpointService.addTouchpoint(this.form_touchpoint.value).subscribe(
      result => {
        console.log(result)
        this.router.navigateByUrl('/', { skipLocationChange: true })
          .then(() => this.router.navigate(['/admin/playsrunning/', this.play._id], { replaceUrl: true }))
        Swal.fire(
          'Good job!',
          'Successfuly Created!',
          'success'
        );
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        }
        this.form_touchpoint.reset();
        this.initializeTouchpoint();
      })

  }



  // -------------------------------------------------------------------------

  // END TOUCHPOINT

  form_endTouchpoint: FormGroup = this.formBuilder.group({
    team: [null, Validators.required],
    team_companies: [null, Validators.required],
    time_to_close: [null, Validators.required],
    worked: [false, Validators.required],
    scorecard: [null, Validators.required],
    responders: ['', Validators.required],
  });

  initializeEndTouchpoint() {
    this.form_endTouchpoint.setValue({
      team: null,
      team_companies: null,
      time_to_close: null,
      worked: false,
      scorecard: null,
      responders: '',
    })
    this.team = false;
    this.team_companies = false;
    this.time_to_close = false;
    this.worked = false;
    this.scorecard = false;
    this.responders = false;
    this.saved = false;
  }

  next_touchpoint(number?) {
    switch (number) {
      case 'team':
        if (this.form_endTouchpoint.controls.team.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.team_companies = true;
          this.team = false;
          console.log(this.form_endTouchpoint.controls.team.value);
        }
        break;

      case 'team_companies':
        if (this.form_endTouchpoint.controls.team_companies.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.team_companies = false;
          this.team = false;
          this.time_to_close = true;
          console.log(this.form_endTouchpoint.controls.team_companies.value);
        }
        break;

      case 'time_to_close':
        if (this.form_endTouchpoint.controls.time_to_close.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.worked = true;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          console.log(this.form_endTouchpoint.controls.time_to_close.value);
        }
        break;

      case 'worked':
        if (this.form_endTouchpoint.controls.worked.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.worked = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = true
          console.log(this.form_endTouchpoint.controls.worked.value);
        }
        break;

      case 'scorecard':
        if (this.form_endTouchpoint.controls.scorecard.invalid === true) {
          alert('Preencha o campo corretamente')
        } else {
          this.worked = false;
          this.time_to_close = false;
          this.team = false;
          this.team_companies = false;
          this.scorecard = false;
          this.responders = true;
          console.log(this.form_endTouchpoint.controls.scorecard.value);
        }
        break;
      default:
        break;
    }
  }

  back_touchpoint(number?) {
    switch (number) {
      case 'team_companies':
        this.team = true;
        this.team_companies = false;
        this.time_to_close = false;
        break;

      case 'time_to_close':
        this.team = false;
        this.team_companies = true;
        this.time_to_close = false;
        break;

      case 'worked':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = true;
        this.worked = false;
        break;

      case 'scorecard':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.worked = true;
        this.scorecard = false;
        break;

      case 'responders':
        this.team = false;
        this.team_companies = false;
        this.time_to_close = false;
        this.worked = false;
        this.scorecard = false;
        this.responders = false;
        break;

      default:
        break;
    }
  }

  endTouchpoint() {

    console.log(this.form_endTouchpoint.value);
    if (this.form_endTouchpoint.controls.responders.invalid === true) {
      alert('Preencha o campo corretamente'), this.initializeEndTouchpoint()
    } else {
      console.log(this.current_touchpoint._id, this.form_endTouchpoint.value);
      this.touchpointService.endTouchpointById(this.current_touchpoint._id, this.form_endTouchpoint.value)
        .pipe()
        .subscribe(
          data => {
            console.log(data)
            this.router.navigateByUrl('/', { skipLocationChange: true })
            .then((() => this.router.navigate(['/admin/playsrunning/', this.play._id], { replaceUrl: true })))
            // .then(() => this.router.navigate(['/admin/playsrunning/', data._id], { replaceUrl: true }))
            Swal.fire(
              'Good job!',
              'Successfuly Ended!',
              'success')
          },
          error => {
            Swal.fire({
              title: 'Oops...',
              text: 'Something went wrong!',
              icon: 'error',
              confirmButtonText: 'Close'
            })
            this.initializeEndTouchpoint()
          });
    }
  }

}
