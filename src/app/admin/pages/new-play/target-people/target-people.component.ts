import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-target-people',
  templateUrl: './target-people.component.html',
  styleUrls: ['./target-people.component.scss']
})
export class TargetPeopleComponent implements OnInit {

  public list = false;
  constructor() { }

  ngOnInit() {
  }
  public open(id?) {
    const list = document.getElementsByClassName('list')[id] as HTMLElement;

    switch (id) {
      case id:
        this.list =! this.list;
        if(this.list) {
          list.style.display = 'block';
        } else {
          list.style.display = 'none';
        }
        break;
    
      default:
        break;
    }    
  }

}
