import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { HelperService } from 'src/app/services/helper.service';
import { PlayService } from 'src/app/services/play.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.scss']
})
export class GoalComponent implements OnInit {

  playAddForm: FormGroup;
  object: string
  howMuch: number
  budget: number;
  start_date = new Date(Date.now());
  expected_end_date = new Date(Date.now())
  namePlay: string
  company_id: string
  type: string
  subType: string
  account_name: string
  account_id: string
  constructor(
    public helper: HelperService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private playService: PlayService,) {

    this.playAddForm = this.formBuilder.group({
      name: '',
      goal: '',
      target: null,
      budget: null,//int
      start_date: new Date(Date.now()),
      expected_end_date: new Date(Date.now()),
    });

  }

  ngOnInit() {
    var getCompanyId = localStorage.getItem('company_id');
    this.company_id = getCompanyId
    var getType = localStorage.getItem('Type');
    this.type = getType
    var getSubType = localStorage.getItem('subType');
    this.subType = getSubType
    var getAcName = localStorage.getItem('account_name');
    this.account_name = getAcName
    var getAcId = localStorage.getItem('account_id');
    this.account_id = getAcId
    console.log(this.company_id, this.type, this.subType, this.account_id)

  }
  getObj(obj) {
    this.object = obj
    console.log(this.object)
  }
  gethowMuch(hm) {
    this.howMuch = hm
    console.log(this.howMuch)
  }
  getbudget(budget) {
    this.budget = budget
    console.log(this.budget)
  }
  getstartDate(startDate) {
    this.start_date = startDate
    console.log(this.start_date)
  }
  getendDate(expected_end_date) {
    this.expected_end_date = expected_end_date
    console.log(this.expected_end_date)
  }
  getnamePlay(namePlay) {
    this.namePlay = namePlay
    console.log(this.namePlay)
  }

  addPlay() {
    const resquest = {
      name: this.namePlay,
      company_id: this.company_id,
      type: this.type,
      subtype: this.subType,
      start_date: this.start_date,
      expected_end_date: this.expected_end_date,
      goal: this.object,
      target: this.howMuch,
      budget: this.budget,
      accounts: [{
        account_id: this.account_id,
        account_name: this.account_name
      }
      ]
    }
    this.playService.addPlay(resquest)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data)
          this.router.navigateByUrl('/', { skipLocationChange: true })
            .then(() => this.router.navigate(['/admin/playsrunning/', data], { replaceUrl: true }))
          // this.router.navigate(['/admin/playsrunning/', data.id], { replaceUrl: true });
          Swal.fire(
            'Great!',
            'Play successfuly created!',
            'success'
          )
          // .then((result) => {
          //   console.log(data)
          //   this.router.navigate(['/admin/playbook'], { replaceUrl: true });
          // });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'No cake for you!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }


}
