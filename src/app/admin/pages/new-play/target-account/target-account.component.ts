import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AccountListService } from 'src/app/services/account-list.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-target-account',
  templateUrl: './target-account.component.html',
  styleUrls: ['./target-account.component.scss']
})
export class TargetAccountComponent implements OnInit {
  accounts: any[];
  accountList: any[]
  isDisabled: boolean = false

  public list = false;

  constructor(private accountListService: AccountListService) {
    this.loadAccountLists();
  }
  getSearch(id) {
    console.log(id)
    localStorage.setItem('account_id', id);
    this.isDisabled = true
  }
  getName(name) {
    localStorage.setItem('account_name', name);
    console.log(name)
  }
  ngOnInit() {
    this.accountListService.getAccountListByCompany().subscribe(dados => { this.accountList = dados })
  }

  public getAccount(index?) {
    console.log(this.accountList)
    console.log(index)
    const list = document.getElementsByClassName('form-check')[index] as HTMLBodyElement;
    console.log("list", list)
    const arrow = document.getElementsByClassName('arrow-account')[index] as HTMLImageElement;
    switch (index) {
      case index:
        this.list = !this.list;
        if (this.list) {
          console.log('abriu');
          list.style.display = 'block';
          arrow.src = './assets/icons/arrow-grey-up.svg';
        } else {
          console.log('fechou');
          list.style.display = 'none';
          arrow.src = './assets/icons/arrow-gray.svg';
        }
        break;
    }
  }

  loadAccountLists() {
    this.accountListService.getAccountListByCompany()
      .pipe(first())
      .subscribe(
        data => { this.accounts = data },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Something went wrong!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });


  }


}