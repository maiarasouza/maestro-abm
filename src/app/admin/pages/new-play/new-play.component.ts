import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-new-play',
  templateUrl: './new-play.component.html',
  styleUrls: ['./new-play.component.scss']
})
export class NewPlayComponent implements OnInit {

  constructor( public helper: HelperService) { }

  ngOnInit() {
  }

}
