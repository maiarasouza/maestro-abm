import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.scss']
})
export class ChooseComponent implements OnInit {
  public preplay: any[] = [
    {
      id: 1,
      text: 'cover',
      title: 'Financial',
    },
    {
      id: 2,
      text: 'cover',
      title: 'Real Estate',
    },
    {
      id: 3,
      text: 'cover',
      title: 'Technology',
    },
    {
      id: 4,
      text: 'cover',
      title: 'Financial',
    },
    {
      id: 5,
      text: 'cover',
      title: 'Real Estate',
    },
    {
      id: 6,
      text: 'cover',
      title: 'Technology',
    },
  ];

  public marketing: any[] = [
    {
      id: 1,
      text: 'cover',
      title: 'Event Play',
    },
    {
      id: 2,
      text: 'cover',
      title: 'Live Event',
    }
  ];

  public sales: any[] = [
    {
      id: 1,
      text: 'cover',
      title: 'Prospect Play',
    },
    {
      id: 2,
      text: 'cover',
      title: 'Ice Breaker',
    },
    {
      id: 2,
      text: 'cover',
      title: 'Sales Play',
    }
  ];

  public cs: any[] = [
    {
      id: 1,
      text: 'cover',
      title: 'Upsell/Crossplay',
    },
    {
      id: 2,
      text: 'cover',
      title: 'Churn Prevention',
    },
    {
      id: 3,
      text: 'cover',
      title: 'Renewal Play',
    },
    {
      id: 4,
      text: 'cover',
      title: 'CS Play',
    }
  ];

  public createmkt = false;
  public createsales = false;
  public createcs = false;
  public createpreplay = false;

  constructor(public router: Router, public helper: HelperService) { }

  ngOnInit() {
    localStorage.setItem('Type', '');
  }

  goToCreate(){
    this.router.navigate(['/admin/newplay/target']);
    this.helper.select('target');
  }
  public openCreate(type) {
    switch (type) {
      case 'p':
        this.createpreplay = !this.createpreplay;
        localStorage.setItem('Type', 'Preplay');
        break;

      case 'm':
        this.createmkt = !this.createmkt;
        localStorage.setItem('Type', 'Marketing');
        break;

      case 's':
        this.createsales = !this.createsales;
        localStorage.setItem('Type', 'Sales');
        break;

      case 'c':
        this.createcs = !this.createcs;
        localStorage.setItem('Type', 'CS');
        break;

      default:
        break;
    }
  }

}
