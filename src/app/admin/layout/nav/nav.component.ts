import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { CompanyService } from 'src/app/services/company.service';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PlayService } from 'src/app/services/play.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  registerUser: FormGroup;
  change: FormGroup;

  public play = false;
  public show: boolean = false;
  public config: boolean = false;
  public hasCompany: boolean = false;
  public plays: any[] = [];

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private userService: UserService, private companyService: CompanyService, private authenticationService: AuthenticationService, private playService: PlayService) {
    this.registerUser = this.formBuilder.group({
      username: ['', Validators.required]
    });
    this.change = this.formBuilder.group({
      password: ['', Validators.required],
      confirmpass: ['']
    });
  }

  ngOnInit() {
  }

  public onplay(close?) {    

    if (close === 'close') {
      this.play = false;
    } else {
      this.play = !this.play;
      this.playService.getPlaysByCompany().subscribe(data => { this.plays = data })
    }
  }

  public onconfig() {
    this.config = !this.config;
  }

  getPlays(index) {
    console.log("index", index)
    const show = document.getElementsByClassName('show')[index] as HTMLBodyElement;
    const arrow = document.getElementsByClassName('arrow-play')[index] as HTMLImageElement;
    switch (index) {
      case index:
        this.show = !this.show;
        if (this.show) {
          console.log('abriu');
          show.style.display = 'block';
          arrow.src = './assets/icons/arrow-up.svg';
        } else {
          console.log('fechou');
          show.style.display = 'none';
          arrow.src = './assets/icons/arrow-down.svg';
        }
        break;
    }
  }

  getPlay(id) {
this.playService.getPlayById(id).subscribe(data => { this.router.navigateByUrl( '/', {skipLocationChange: true}).then(()=> this.router.navigate(['/admin/playsrunning/', data._id], { replaceUrl: true }))})
    // this.playService.getPlayById(id).subscribe(data => { this.router.navigate(['/admin/playsrunning/', data._id], { replaceUrl: true }) })
    // `/admin/playsrunning/${data._id}`
  }

  public logout() {
    confirm('Deseja realmente sair?');
    this.authenticationService.logout();
    this.router.navigate(['/auth/login']);
  }

  changePassword() {
    this.userService.changePassword(this.p.password.value)
      .pipe(first())
      .subscribe(
        data => {
          alert("Success!")
        },
        error => {
          alert("Error!")
        });
  }

  addUser() {
    this.companyService.addUser(this.f.username.value)
      .pipe(first())
      .subscribe(
        data => {
          alert("Success!")
        },
        error => {
          alert("Error!")
        });
  }

  get f() { return this.registerUser.controls; }

  get p() { return this.change.controls; }


  search(a: string) {
    a = a ? a.trim() : '';
    this.plays = this.plays.filter(fa => fa.name.toLowerCase().includes(a.toLowerCase()));
    console.log(a, this.plays);
  }
}
