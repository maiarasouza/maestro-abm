import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  // @ViewChild('drawer', { static: false }) public drawer: MatDrawer;
  public nav = true;
  constructor(
    public helper: HelperService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { 
    if (localStorage.getItem('token') == null) {
      this.router.navigate(['/auth/login']);
    }
  }

  ngOnInit() { 
    if (this.helper.isMobile) {
      this.nav = false;
    }
  }

  // ngAfterViewInit() {
  //   this.opentoggle();
  // }

  public opentoggle() {
    // if (this.helper.isMobile) {
      this.nav =! this.nav;
    // }
  }

}
