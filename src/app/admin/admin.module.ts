import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NavComponent } from './layout/nav/nav.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AccountComponent } from './pages/account/account.component';
import { PlaybookComponent } from './pages/playbook/playbook.component';
import { PlaysrunningComponent } from './pages/playsrunning/playsrunning.component';
import { AccountListComponent } from './pages/account/account-list/account-list.component';
import { ContactComponent } from './pages/account/contact/contact.component';
import { ContaComponent } from './pages/account/conta/conta.component';
import { ContactListComponent } from './pages/account/contact-list/contact-list.component';
import { ListsSpecComponent } from './pages/account/lists-spec/lists-spec.component';
import { IntegrationsComponent } from './pages/integrations/integrations.component';
import { CompanyService } from '../services/company.service';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '../auth/jwt.interceptor';
import { ErrorInterceptor } from '../auth/error.interceptor';
import { ListsAddComponent } from './pages/account/lists-add/lists-add.component';
import { AccountListService } from '../services/account-list.service';
import { AccountAddComponent } from './pages/account/account-add/account-add.component';
import { AccountService } from '../services/account.service';
import { ContactListService } from '../services/contact-list.service';
import { ContactService } from '../services/contact.service';
import { AccountItemComponent } from './pages/account/account/account.component';
import { ContactAddComponent } from './pages/account/contact-add/contact-add.component';
import { ContactListAddComponent } from './pages/account/contact-list-add/contact-list-add.component';
import { FirstEntryComponent } from './pages/first-entry/first-entry.component';
import { NewPlayComponent } from './pages/new-play/new-play.component';
import { TargetComponent } from './pages/new-play/target/target.component';
import { GoalComponent } from './pages/new-play/goal/goal.component';
import { TargetAccountComponent } from './pages/new-play/target-account/target-account.component';
import { TargetPeopleComponent } from './pages/new-play/target-people/target-people.component';
// import { FormtouchpointComponent } from '../widgets/forms/formtouchpoint/formtouchpoint.component';
import { TouchpointActions } from "../model/touchpointActions"
import { MAT_DIALOG_DATA } from '@angular/material';
import { ChooseComponent } from './pages/new-play/choose/choose.component';


@NgModule({
  declarations: [
    AdminComponent,
    NavComponent,
    DashboardComponent,
    AccountComponent,
    PlaybookComponent,
    PlaysrunningComponent,
    AccountListComponent,
    AccountAddComponent,
    AccountItemComponent,
    ListsAddComponent,
    ContactComponent,
    ContactAddComponent,
    ContactListAddComponent,
    ContaComponent,
    ContactListComponent,
    ListsSpecComponent,
    IntegrationsComponent,
    FirstEntryComponent,
    NewPlayComponent,
    TargetComponent,
    GoalComponent,
    TargetAccountComponent,
    TargetPeopleComponent,
    // FormtouchpointComponent,
    ChooseComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    // FormtouchpointComponent,
  ],
  providers: [
    AuthenticationService,
    UserService,
    CompanyService,
    AccountListService,
    AccountService,
    ContactListService,
    ContactService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    { provide: MAT_DIALOG_DATA, useValue: [] }
  ],
  entryComponents: [
    // FormtouchpointComponent,
  ],

})
export class AdminModule { }
