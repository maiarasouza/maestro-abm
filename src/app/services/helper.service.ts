import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { BehaviorSubject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  public navType = 'nav-trasparent';
  public hasFooter = true;
  public hasNav = true;
  public isMobile: boolean;
  public loading = new BehaviorSubject(false);
  public navOpen: boolean;
  public content = 'nav';

  public userToLogin: any;
  public drawer: MatDrawer;

  public action = false;
  public actionList = false;

  public mob;

  public form: FormGroup = this.fb.group({
    radio: ['', Validators.required],
    role: ['', Validators.required],
  });

  constructor(
    private breakpointObserver: BreakpointObserver,
    public http: HttpClient,
    private fb: FormBuilder
  ) {
    this.breakpointObserver.observe(['(min-width: 767px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.isMobile = false;
          this.mob = this.isMobile;
        } else {
          this.isMobile = true;
          this.mob = this.isMobile;
        }
      });
  }

  public scroll() {
    parent.scroll(0, 0);
  }

  public smooth(id) {
    document.getElementById(id).scrollIntoView({behavior:"smooth"});
  }
  
  get isLoading() {
    return this.loading.getValue();
  }

  public openAction(type?) {
      // this.action = !this.action;
  }

  public new() {
    alert('Em breve');
  }

  public select(item?) {
    const imgTemplate = document.getElementsByClassName('imgLink')[0] as HTMLImageElement;
    const imgTarget = document.getElementsByClassName('imgLink')[1] as HTMLImageElement;
    const imgGoal = document.getElementsByClassName('imgLink')[2] as HTMLImageElement;
    const txtTemplate = document.getElementsByClassName('txt-template')[0] as HTMLBodyElement;
    const txtTarget = document.getElementsByClassName('txt-template')[1] as HTMLBodyElement;
    const txtGoal = document.getElementsByClassName('txt-template')[2] as HTMLBodyElement;

    switch (item) {
      case 'template':
        imgTemplate.src = './assets/icons/elipse-orange.svg';
        txtTemplate.style.color = '#ff5f39';
        imgTarget.src = './assets/icons/elipse-white.svg';
        txtTarget.style.color = '#ffffff';
        imgGoal.src = './assets/icons/elipse-white.svg';
        txtGoal.style.color = '#ffffff';
        break;
      case 'target':
        imgTemplate.src = './assets/icons/elipse-checked.svg';
        txtTemplate.style.color = '#ffffff';
        imgTarget.src = './assets/icons/elipse-orange.svg';
        txtTarget.style.color = '#ff5f39';
        imgGoal.src = './assets/icons/elipse-white.svg';
        txtGoal.style.color = '#ffffff';
        break;
      case 'goal':
        imgTemplate.src = './assets/icons/elipse-checked.svg';
        txtTemplate.style.color = '#ffffff';
        imgTarget.src = './assets/icons/elipse-checked.svg';
        txtTarget.style.color = '#ffffff';
        imgGoal.src = './assets/icons/elipse-orange.svg';
        txtGoal.style.color = '#ff5f39';
        break;

      default:
        break;
    }
  }
}
