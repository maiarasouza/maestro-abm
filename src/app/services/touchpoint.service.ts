import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { TouchpointModel } from '../model/touchpoint.interface';

@Injectable({
  providedIn: 'root'
})
export class TouchpointService {

  constructor(private http: HttpClient) { }

  baseUrl: string = "/api"

  addTouchpoint(touchpoint) {
    console.log("dentro do service", touchpoint)
    return this.http.post<any>(this.baseUrl + '/touchpoint', touchpoint)
      .pipe(map(response => {
        return response;
      }));
  }

  getgetAllTouchpointAllPlay() {
    return this.http.get<any>(this.baseUrl + '/touchpoint/')
      .pipe(map(response => {
        return response;
      }));
  }

  getAllTouchpointByCompany(company_id) {
    return this.http.get<any>(this.baseUrl + '/touchpoint/get-all-plays-by-company/' + company_id)
      .pipe(map(response => {
        console.log(response)
        return response;
      }));
  }

  getAllTouchpointByPlay(play_id) {
    return this.http.get<any>(this.baseUrl + '/touchpoint/get-all-touchpoint-by-play/' + play_id)
      .pipe(map(response => {
        console.log(response)
        return response;
      }));
  }

  getOneTouchpoint(id: any) {
    return this.http.get<any>(this.baseUrl + '/touchpoint/' + id)
      .pipe(map(response => {
        return response;
      }));
  }

  updateTouchpointById(id: String, touchpoint: TouchpointModel) {
    return this.http.patch<any>(this.baseUrl + '/touchpoint/' + id, { touchpoint: touchpoint })
      .pipe(map(response => {
        return response;
      }));
  }

  endTouchpointById(id: string, learning_touchpoint: {}) {
    return this.http.put<any>(this.baseUrl + '/touchpoint/end-touchpoint/' + id, { learning_touchpoint: learning_touchpoint })
      .pipe(map(response => {
        return response;
      }));
  }

  deleteTouchpointById(touchpoint_id: string) {
    return this.http.delete<any>(this.baseUrl + '/touchpoint/' + touchpoint_id)
      .pipe(map(response => {
        return response;
      }));
  }

  addContact(id: string, contact_id: string, contact_name: string) {
    return this.http.put<any>(this.baseUrl + '/touchpoint/add-contacts', { id: id, contact_id: contact_id, contact_name: contact_name })
  }

  deleteContact(id: string, contact_id: string) {
    return this.http.put<any>(this.baseUrl + '/touchpoint/delete-contact', { id: id, contact_id: contact_id })
  }

}
