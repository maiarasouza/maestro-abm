﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Company } from '../model/company';
import jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class CompanyService {
    constructor(private http: HttpClient) { }

    baseUrl: string = "/api"
    token: string;
    company_id: string;
    username: string;


    addUser(username: string) {
        this.company_id = window.localStorage.getItem('company_id')
        return this.http.put<any>(this.baseUrl + '/company/add-username', { username, id: this.company_id })
            .pipe(map(response => {
                return response;
            }));
    }

    registerCompany(company: Company) {
        company.username = window.localStorage.getItem('username')
        //todo get username from local storage e setar na company
        return this.http.post<any>(this.baseUrl + '/company', company)
            .pipe(map(response => {
                //let tokenInfo = this.getDecodedAccessToken(response.data);

                localStorage.setItem('company_id', response.generatedId._id);
                console.log("company id-> " + response.generatedId._id);
                return response;
            }));
    }


    updateCompany(company: Company): Observable<any> {
        company.username = window.localStorage.getItem('username')
        this.company_id = window.localStorage.getItem('company_id')

        return this.http.patch<any>(this.baseUrl + '/company/' + this.company_id,
            {
                name: company.name,
                ein: company.ein,
                n_of_employees: company.n_of_employees,
                revenue: company.revenue,
                industry: company.industry,
                description: company.industry,
                geolocation: company.geolocation
            })
    }


    getCompanyById() {
        this.company_id = window.localStorage.getItem('company_id')
        return this.http.get<any>(this.baseUrl + '/company/' + this.company_id)
            .pipe(map(response => {
                return response;
            }));
    }

    getDecodedAccessToken(token: string): any {
        try{
            return jwt_decode(token);
        }
        catch(Error){
            return null;
        }
      }

}
