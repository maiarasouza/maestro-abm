import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Company } from '../model/company';
import { AccountList } from '../model/accountList';
import { AccountItem } from '../model/accountItem';
import { AccountEntityItem } from '../model/accountEntityItem';
import { Contact } from '../model/contact';
import { Content } from '@angular/compiler/src/render3/r3_ast';

@Injectable({ providedIn: 'root' })
export class AccountService {
    constructor(private http: HttpClient) { }

    baseUrl: string = "/api"
    token: string;
    company_id: string;
    username: string;
    content: AccountEntityItem;


    addAccount(account: AccountEntityItem) {
        account.company_id = window.localStorage.getItem('company_id');

        return this.http.post<any>(this.baseUrl + '/accounts', account)
            .pipe(map(response => {
                return response.id;
            }));
    }

    addAccountInList(accountListId: string, accountItem: any) {
        return this.http.put<any>(this.baseUrl + '/account-list/add-account', {
            account_id: accountItem._id,
            id: accountListId,
            account_name: accountItem.name
        })

            .pipe(map(response => {
                return response;
            }));
    }

    addContactInList(accountId: string, contact: any) {
        return this.http.put<any>(this.baseUrl + '/accounts/add-contact', {
            contact_id: contact._id,
            id: accountId,
            contact_name: contact.name
        })

            .pipe(map(response => {
                return response;
            }));
    }

    deleteAccountById(accountId: string) {
        return this.http.delete<any>(this.baseUrl + '/accounts/' + accountId)
            .pipe(map(response => {
                return response;
            }));
    }

    updatetAccountById(account: AccountEntityItem) {
        return this.http.patch<any>(this.baseUrl + '/accounts/' + account._id, {
            name: account.name,
            description: account.description,
            geolocation: account.geolocation,
            industry: account.industry,
            website: account.website,
            revenue: account.revenue,
            engagement: account.engagement,
            page_views: account.page_views
        })
            .pipe(map(response => {
                return response;
            }));
    }

    getAccountByCompany() {
        this.company_id = window.localStorage.getItem('company_id');
        return this.http.get<any>(this.baseUrl + '/accounts/get-all-accounts-by-company/' + this.company_id)
            .pipe(map(response => {
                return response;
            }));
    }

    getAccountsByPlay(play_id) {
        return this.http.get<any>(this.baseUrl + '/accounts/get-all-accounts-by-play/' + play_id)
            .pipe(map(response => {
                return response
            }))
    }

    getContactsByPlay(play_id) {
        this.getAccountsByPlay(play_id).subscribe(
            result => {
                console.log(result)
            }
        )
    }

    getAccountsByCompany() {
        this.company_id = window.localStorage.getItem('company_id');
        return this.http.get<any>(this.baseUrl + '/accounts/get-all-accounts-by-company/' + this.company_id)
            .pipe(map(response => {
                return response;
            }));
    }

    getAccountById(idAcc: any) {
        return this.http.get<any>(this.baseUrl + '/accounts/' + idAcc)
            .pipe(map(response => {
                return response;
            }));
    }

    registerCompany(company: Company) {
        company.username = window.localStorage.getItem('username')
        //todo get username from local storage e setar na company
        return this.http.post<any>(this.baseUrl + '/company', company)
            .pipe(map(response => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes

                //todo retornar ok e redirecionar para login
                return response;
            }));
    }

}