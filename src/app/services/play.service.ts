import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Play } from '../model/play';

@Injectable({ providedIn: 'root' })
export class PlayService {
    constructor(private http: HttpClient) { }

    baseUrl: string = "/api"
    token: string;
    play_id: string;
    company_id: string;
    username: string;


    addPlay(play) {
        play.company_id = window.localStorage.getItem('company_id');
        return this.http.post<any>(this.baseUrl + '/play', play)
            .pipe(map(response => {
                console.log(response._id)
                return response._id;
            }));
    }

    deletePlayById(playId: string) {
        return this.http.delete<any>(this.baseUrl + '/play/' + playId)
            .pipe(map(response => {
                console.log(response)
                return response;
            }));
    }

    updatePlayById(id: String, playData) {
        return this.http.patch<any>(this.baseUrl + '/play/' + id, playData)
            .pipe(map(response => {
                return response;
            }));
    }

    getPlaysByCompany() {
        console.log(window.localStorage.getItem('company_id'))
        this.company_id = window.localStorage.getItem('company_id');
        return this.http.get<any>(this.baseUrl + '/play/get-all-plays-by-company/' + this.company_id)
            .pipe(map(response => {
                console.log(response)
                return response;
            }));
    }

    getPlayById(id: string) {
        return this.http.get<any>(this.baseUrl + '/play/' + id)
            .pipe(map(response => {
                return response;
            }));
    }
    getAllPlay() {
        return this.http.get<any>(this.baseUrl + '/play/')
            .pipe(map(response => {
                return response;
            }));
    }

    endPlayById(id, learning) {
        return this.http.put<any>(this.baseUrl + '/play/end-play/' + id, { "learning_play": learning })
            .pipe(map(response => {
                console.log(response)
                return response;
            }));
    }

    getUniqueTouchbyId(idTouch: any) {
        return this.http.get<any>(this.baseUrl + '/touchpoint/' + idTouch)
            .pipe(map(response => {
                return response;
            }));
    }

}