import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Company } from '../model/company';
import { AccountList } from '../model/accountList';
import { AccountItem } from '../model/accountItem';
import { Contact } from '../model/contact';

@Injectable({ providedIn: 'root' })
export class AccountListService {
    constructor(private http: HttpClient) { }

    baseUrl: string =  "/api"
    token: string;
    company_id:string;
    username:string;

   
    addAccountList(accountList: AccountList) {
        accountList.company_id = window.localStorage.getItem('company_id');
        accountList.created_by = window.localStorage.getItem('username');
        return this.http.post<any>(this.baseUrl + '/account-list', accountList)
            .pipe(map(response => {                
                return response.id;
            }));
    }

    addAccountInList(account: AccountItem) {
        return this.http.put<any>(this.baseUrl + '/account-list/add-account', account)
            .pipe(map(response => {                
                return response;
            }));
    }
    
    deleteAccountListById(accounlistId:string) {
        return this.http.delete<any>(this.baseUrl + '/account-list/'+ accounlistId)
            .pipe(map(response => {                
                return response;
            }));
    }

    updatetAccountListById(accountList: AccountList) : Observable<any> {
       
        accountList.favorite = String(accountList.favorite)        
        accountList.company_id = window.localStorage.getItem('company_id');
        accountList.created_by = window.localStorage.getItem('username');
        return this.http.patch<any>(this.baseUrl + '/account-list/'+accountList._id,{ 
                                                        name: accountList.name,
                                                        type: accountList.type,
                                                        favorite: accountList.favorite} );
    }


    getAccountListByCompany(){
        this.company_id = window.localStorage.getItem('company_id');
        return this.http.get<any>(this.baseUrl + '/account-list/get-all-accountlist-by-company/'+ this.company_id )
        .pipe(map(response => {    
                     
            return response;
           
        }));
    }

    getAccountListById(idAccList: any){        
        return this.http.get<any>(this.baseUrl + '/account-list/'+ idAccList )
        .pipe(map(response => {                
            return response;
        }));
    }

    registerCompany(company: Company) {
        company.username = window.localStorage.getItem('username')
        //todo get username from local storage e setar na company
        return this.http.post<any>(this.baseUrl + '/company', company )
            .pipe(map(response => {
                                // store user details and jwt token in local storage to keep user logged in between page refreshes
               
                //todo retornar ok e redirecionar para login
                return response;
            }));
    }
    
}