import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../model/user';
import { Company } from '../model/company';
import { AccountList } from '../model/accountList';
import { AccountItem } from '../model/accountItem';
import { ContactList } from '../model/contactList';

@Injectable({ providedIn: 'root' })
export class ContactListService {
    constructor(private http: HttpClient) { }

    baseUrl: string =  "/api"
    token: string;
    company_id:string;
    username:string;

   
    addContactList(contactList: ContactList) {
        contactList.company_id = window.localStorage.getItem('company_id');
        contactList.created_by = window.localStorage.getItem('username');
        return this.http.post<any>(this.baseUrl + '/contact-list', contactList)
            .pipe(map(response => {                
                return response.id;
            }));
    }

    addAccountInList(account: AccountItem) {
        return this.http.put<any>(this.baseUrl + '/account-list/add-account', account)
            .pipe(map(response => {                
                return response;
            }));
    }

    deleteAccount(accounlistId:string, accountId: string) {
        return this.http.put<any>(this.baseUrl + '/account-list/delete-account', {id:accounlistId , account_id:accountId})
            .pipe(map(response => {                
                return response;
            }));
    }

    deleteContactListById(contactListId:string) {
        return this.http.delete<any>(this.baseUrl + '/contact-list/'+ contactListId)
            .pipe(map(response => {                
                return response;
            }));
    }

    updateContactListById(contactList: ContactList): Observable<any>  {
        contactList.favorite = String(contactList.favorite)    
        contactList.company_id = window.localStorage.getItem('company_id');
        contactList.created_by = window.localStorage.getItem('username');
        return this.http.patch<any>(this.baseUrl + '/contact-list/'+contactList._id,{ 
                                                        name: contactList.name,
                                                        type: contactList.type,
                                                        favorite: contactList.favorite} );
    }

    getContactListByCompany(){
        this.company_id = window.localStorage.getItem('company_id');
        return this.http.get<any>(this.baseUrl + '/contact-list/get-all-contactlist-by-company/'+ this.company_id )
        .pipe(map(response => {                
            return response;
        }));
    }

    getAccountListById(idContactList: any){        
        return this.http.get<any>(this.baseUrl + '/contact-list/'+ idContactList )
        .pipe(map(response => {                
            return response;
        }));
    }

    registerCompany(company: Company) {
        company.username = window.localStorage.getItem('username')
        //todo get username from local storage e setar na company
        return this.http.post<any>(this.baseUrl + '/company', company )
            .pipe(map(response => {
                                // store user details and jwt token in local storage to keep user logged in between page refreshes
               
                //todo retornar ok e redirecionar para login
                return response;
            }));
    }
    
}