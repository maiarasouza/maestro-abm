import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import jwt_decode from 'jwt-decode';

import { User } from '../model/user';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<string>;
    public currentUser: Observable<string>;
    baseUrl: string =  "/api"

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<string>(localStorage.getItem('username'));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): string {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(this.baseUrl+'/users/login', { username, password })
            .pipe(map(response => {
                let tokenInfo = this.getDecodedAccessToken(response.data);
                localStorage.setItem('username', tokenInfo.username);
                localStorage.setItem('name', tokenInfo.name);
                localStorage.setItem('surname', tokenInfo.surname);
                localStorage.setItem('company_id', tokenInfo.company_id);
                localStorage.setItem('token', response.data);
                // store user details and jwt token in local storage to keep user logged in between page refreshes
               console.log("username-> "+tokenInfo.username);
               console.log("name-> "+tokenInfo.name);
               console.log("surname-> "+tokenInfo.surname);
               console.log("company id-> "+tokenInfo.company_id);

                this.currentUserSubject.next(tokenInfo.username);
                return tokenInfo.username;
            }));
    }

    changePassword(username: string, password: string) {
        return this.http.post<any>(this.baseUrl+'/users/change-password', { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('username');
        localStorage.removeItem('company_id');
        localStorage.removeItem('token');
        this.currentUserSubject.next(null);
    }

    addNewUser(name: string, surname:string, username: string, password: string) { 
        return this.http.post<any>(this.baseUrl + '/users/register', {name, surname, username , password })
            .pipe(map(response => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('currentUser', JSON.stringify(user));
                //todo retornar ok e redirecionar para login
                return response;
            }));
    }

    getDecodedAccessToken(token: string): any {
        try{
            return jwt_decode(token);
        }
        catch(Error){
            return null;
        }
      }
}