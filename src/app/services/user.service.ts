﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../model/user';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    baseUrl: string =  "/api"
    token: string;
    company_id: string; 

    
   
    changePassword(password: string) {        
        this.token = window.localStorage.getItem('token')
        return this.http.post<any>(this.baseUrl + '/users/change-password', { token : this.token , password })
            .pipe(map(response => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('currentUser', JSON.stringify(user));
                //todo retornar ok e redirecionar para login
                return response;
            }));
    }

    
}
