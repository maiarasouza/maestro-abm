import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from '../admin/admin-routing.module';
import { AdminComponent } from '../admin/admin.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormtouchpointComponent } from './forms/formtouchpoint/formtouchpoint.component';


@NgModule({
  declarations: [
    FormtouchpointComponent,
    AdminComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    // AdminComponent
  ],

})
export class WidgetsModule { }
