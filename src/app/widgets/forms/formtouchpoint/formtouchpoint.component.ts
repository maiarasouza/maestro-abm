import { Component, OnInit, Inject } from '@angular/core'
import { ActivatedRoute, Router } from "@angular/router"
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms"
import { TouchpointActions } from "../../../model/touchpointActions"
import { TouchpointModel } from "../../../model/touchpoint.interface"
import Swal from "sweetalert2"
import { TouchpointService } from "../../../services/touchpoint.service"
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material'


@Component({
  selector: 'app-formtouchpoint',
  templateUrl: './formtouchpoint.component.html',
  styleUrls: ['./formtouchpoint.component.scss'],
  providers: [
    TouchpointActions,
    // { provide: MAT_DIALOG_DATA, useValue: [] }
  ]
})
export class FormtouchpointComponent implements OnInit {

  touchpointModel: TouchpointModel;
  openwink = false;
  openpoke = false;
  openstalk = false;
  openflirt = false;
  toggle = false;

  constructor(
    public touchpointService: TouchpointService,
    private matDialog: MatDialog,
    public route: ActivatedRoute,
    public router: Router,
    public touchpointActions: TouchpointActions,
    @Inject(MAT_DIALOG_DATA) public touchpoint,
  ) { this.populateFormTouchpoint(touchpoint) }

  ngOnInit() {
  }

  onClear() {
    this.form_touchpoint.reset();
    this.initializeFormGroupTouchpoint();
  }

  onClose() {
    this.form_touchpoint.reset();
    this.initializeFormGroupTouchpoint();
    this.matDialog.closeAll();
  }

  initializeFormGroupTouchpoint() {
    this.form_touchpoint.setValue({
      name: '',
      play_id: '',
      start_date: Date.now(),
      end_date: '',
      channel: '',
      for_who: [],
      type: [],
      description: '',
      responsibles: [],
      triggers: [],
      icebreaker: [],
      communication: [],
      attachments: [],
    })
  }

  onSubmit() {
    if (this.form_touchpoint.valid) {
      this.touchpointService.addTouchpoint(this.form_touchpoint.value).subscribe(
        result => {
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          );
          error => {
            Swal.fire({
              title: 'Oops...',
              text: 'Something went wrong!',
              icon: 'error',
              confirmButtonText: 'Close'
            })
          }
          this.form_touchpoint.reset();
          this.initializeFormGroupTouchpoint();
          this.matDialog.closeAll();
        })
    }
  }

  populateFormTouchpoint(touchpoint) {
    this.initializeFormGroupTouchpoint();
    console.log(touchpoint.play_id)
    this.form_touchpoint.get('name').setValue(touchpoint.name)
    this.form_touchpoint.get('play_id').setValue(touchpoint.play_id)
    this.form_touchpoint.get('start_date').setValue(new Date(touchpoint.start_date))
    this.form_touchpoint.get('end_date').setValue(new Date(touchpoint.end_date))
    this.form_touchpoint.get('channel').setValue(touchpoint.channel)
    this.form_touchpoint.get('for_who').setValue(touchpoint.for_who)
    this.form_touchpoint.get('type').setValue(touchpoint.type)
    this.form_touchpoint.get('description').setValue(touchpoint.description)
    this.form_touchpoint.get('responsibles').setValue(touchpoint.responsibles)
    this.form_touchpoint.get('triggers').setValue(touchpoint.triggers)
    this.form_touchpoint.get('icebreaker').setValue(touchpoint.icebreaker)
    this.form_touchpoint.get('communication').setValue(touchpoint.communication)
    this.form_touchpoint.get('attachments').setValue(touchpoint.attachments)
  }

  form_touchpoint: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    play_id: new FormControl('', Validators.required),
    start_date: new FormControl('', Validators.required),
    end_date: new FormControl(''),
    channel: new FormControl(''),
    for_who: new FormControl([]),
    type: new FormControl([]),
    description: new FormControl(''),
    responsibles: new FormControl([]),
    triggers: new FormControl([]),
    icebreaker: new FormControl([]),
    communication: new FormControl([]),
    attachments: new FormControl([]),
  })

  getAction(event) {
    localStorage.setItem('Action', event);
    console.log(event)
  }

  public openAction(action) {
    switch (action) {
      case 'w':
        this.openwink = !this.openwink;
        localStorage.setItem('Action', 'Wink');
        break;

      case 'p':
        this.openpoke = !this.openpoke;
        localStorage.setItem('Action', 'Poke');
        break;

      case 's':
        this.openstalk = !this.openstalk;
        localStorage.setItem('Action', 'Stalk');
        break;

      case 'f':
        this.openflirt = !this.openflirt;
        localStorage.setItem('Action', 'Flirt');
        break;

      default:
        break;
    }
  }

  public toggleArrow(numero) {
    console.log("funcao onpenTouch");
    const s = document.getElementsByClassName("toggle")[numero] as HTMLBodyElement;
    switch (numero) {
      case numero:
        this.toggle = !this.toggle;
        if (this.toggle) {
          s.style.display = "block";
        } else {
          s.style.display = "none";
        }
        break;
    }
  }

}
