import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebsiteRoutingModule } from './website-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { WebsiteComponent } from './website.component';
import { NavComponent } from './layout/nav/nav.component';


@NgModule({
  declarations: [
    WebsiteComponent,
    HomeComponent,
    NavComponent
  ],
  imports: [
    CommonModule,
    WebsiteRoutingModule
  ]
})
export class WebsiteModule { }
