import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public menu: boolean = true;

  constructor( public helper: HelperService) { }

  ngOnInit() {
    if (this.helper.isMobile === true) {
      this.menu = false;
    }
  }


  abrirMenu() {
    if (this.helper.isMobile === true) {
      this.menu = !this.menu;
      const body = document.getElementsByTagName('body')[0] as HTMLElement;
      if (this.menu === true) {
        body.style.overflow = 'hidden';
      } else {
        body.style.overflow = 'auto';
      }
    }
  }

}
