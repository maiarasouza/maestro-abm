export class AccountList {
    _id: string;
    name: string;
    company_id: string;
    number_accounts: number;
    type: string;
    accounts: [{account_id: string, account_name: string}];
    created_by: string;
    created_since: Date;
    updated_in: Date;
    favorite: string;
}