export class PlaybookTemplates {
    public awareness: any[] = [
        {
            id: 1,
            text: 'cover',
            name: 'Ebook',
        },
        {
            id: 2,
            text: 'cover',
            name: 'Webinar',
        },
        {
            id: 3,
            text: 'cover',
            name: 'Podcast',
        },
        {
            id: 4,
            text: 'cover',
            name: 'Community Action',
        },
        {
            id: 5,
            text: 'cover',
            name: 'Meetup',
        },
        {
            id: 6,
            text: 'cover',
            name: 'Lobby',
        },
    ];

    public preplay: any[] = [
        {
            id: 1,
            text: 'cover',
            name: 'Strategic Announcement',
        },
        {
            id: 2,
            text: 'cover',
            name: 'Incoming News',
        },
        {
            id: 3,
            text: 'cover',
            name: 'Buzz & Gossip',
        },
        {
            id: 4,
            text: 'cover',
            name: 'Blog',
        },
        {
            id: 5,
            text: 'cover',
            name: 'Fomo Strategy',
        },
        {
            id: 6,
            text: 'cover',
            name: 'Lobby',
        },
    ];

    public marketing: any[] = [
        {
            id: 1,
            text: 'cover',
            name: 'Online Video',
        },
        {
            id: 2,
            text: 'cover',
            name: 'Live Event',
        }
    ];

    public sales: any[] = [
        {
            id: 1,
            text: 'cover',
            name: 'Prospect Play',
        },
        {
            id: 2,
            text: 'cover',
            name: 'Ice Breaker',
        },
        {
            id: 3,
            text: 'cover',
            name: 'Sales Play',
        }
    ];

    public cs: any[] = [
        {
            id: 1,
            text: 'cover',
            name: 'Upsell/CrossSell',
        },
        {
            id: 2,
            text: 'cover',
            name: 'Churn Prevention',
        },
        {
            id: 3,
            text: 'cover',
            name: 'Renewal Play',
        },
        {
            id: 4,
            text: 'cover',
            name: 'CS Play',
        }
    ];
}