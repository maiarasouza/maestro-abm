export class AccountEntityItem {
    _id: string;
    name: string;
    description: string;
    company_id: string;
    geolocation: string;
    industry: string;
    n_of_employees: number;
    website: string;
    revenue: number;
    engagement: number;
    page_views: number;    
    contacts: [{contact_id: string, contact_name: string}];
    plays: [{play_id: string, play_name: string}];
}
