export class Contact {
    _id: string;
    name: string;
    description: string;
    external: boolean;
    company_id: string;
    geolocation: string;
    industry: string;
    email: string;
    phone: string;
    linkedin: string;
    website: string;
    accounts: [{account_id: string, account_name: string}];
    role: string;
    plays: [{play_id: string, play_name: string}];
}