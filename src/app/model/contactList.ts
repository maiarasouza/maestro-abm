export class ContactList {
    _id: string;
    name: string;
    company_id: string;
    number_contacts: boolean;
    type: string;
    created_by: string;
    created_since: string;
    updated_in: string;
    favorite: string;
    contacts: [{ contact_id: string, contact_name: string }];
}