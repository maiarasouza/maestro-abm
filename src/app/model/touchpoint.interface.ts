export interface TouchpointModel {

    _id: string;
    company_id: string;
    play_id: string;
    name: string;
    status: boolean;
    behaviour: string;
    for_who: [{ account_id: string, account_name: string }]; //lista de contacts
    created_since: Date;
    start_date: Date;
    end_date: Date;
    updated_in: Date;
    type: string[];
    description: string;
    responsibles: string[];
    triggers: string[];
    icebreaker: string[];
    action: string[];
    attachments: string[];
    worked: boolean;
    grade: number;
    who_answered: string[];
    contacts: [{ contact_id: string, contact_name: string }];
    learning_touchpoint: [{
        team: number;
        team_companies: number;
        time_to_close: number;
        worked: boolean;
        scorecard: number;
        responders: string;
    }];

}