export class TouchpointActions {

    public wink: any[] = [
        {
            id: 1,
            name: 'Email to prospect',
        },
        {
            id: 2,
            name: 'Email from your boss',
        },
        {
            id: 3,
            name: 'Email from the CEO',
        },
        {
            id: 4,
            name: 'Email from a board member or respected analyst/friend',
        }
    ];

    public poke: any[] = [
        {
            id: 1,
            name: 'Call the prospect but don’t leave a voicemail',
        },
        {
            id: 2,
            name: 'Call the prospect and leave a voicemail if no answer',
        },
        {
            id: 3,
            name: 'Ghost call',
        },
        {
            id: 4,
            name: 'Send a text message',
        },
        {
            id: 5,
            name: 'Send a personalized video (using technology like Vidyard)',
        }
    ];

    public stalk: any[] = [
        {
            id: 1,
            name: 'View the prospect’s LinkedIn profile',
        },
        {
            id: 2,
            name: 'Connect with the prospect via LinkedIn',
        },
        {
            id: 3,
            name: 'Send Inmail via LinkedIn',
        },
        {
            id: 4,
            name: 'Follow the prospect on Twitter',
        },
        {
            id: 5,
            name: 'Comment on a blog post the prospect wrote',
        },
        {
            id: 6,
            name: 'Comment on a LinkedIn post the prospect wrote (or one he/she commented on)',
        },
        {
            id: 7,
            name: 'Download content from the prospect’s Website',
        },
        {
            id: 8,
            name: 'Target the prospect with custom, campaign-specific ads on LinkedIn (LinkedIn Matched Audiences), Google (Customer Match) or Facebook (Custom Audiences)',
        }
    ];

    public flirt: any[] = [
        {
            id: 1,
            name: 'Send low-cost direct mail (e.g. postcard)',
        },
        {
            id: 2,
            name: 'Send low-cost gift (e.g. Starbucks gift card)',
        },
        {
            id: 3,
            name: 'Send high-cost gift (e.g. bottle of wine)',
        },
        {
            id: 4,
            name: 'Send company swag (e.g. t-shirt, water bottle)',
        },
        {
            id: 5,
            name: 'Send gift “teaser” (e.g. send iPad case, request meeting to get the iPad)',
        }
    ];
}