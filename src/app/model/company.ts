export class Company {
    id: string;
    name: string;
    ein: string;
    username: string;
    n_of_employees: number;
    revenue: number;
    industry: string;
    description: string;
    geolocation:string;
}