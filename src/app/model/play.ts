export class Play {
    id: string;
    name: string;

    expected_end_date: Date;
    goal: string;
    target: number;
    budget: number;
    account:[{
        account_id: string,
        account_name:string
    }];

    company_id: string;
    touchpoints: [{touchpoint_id: string, touchpoint_name: string}];
    start_date: Date;
    end_date: Date;
    status: boolean;
    contacts: number; 
    learning_play: string;
}