import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyService } from '../../services/company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private companyService: CompanyService
  ) {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      ein: ['', Validators.required],      
      n_of_employees: ['', Validators.required],//int
      revenue: ['', Validators.required],//int
      industry: ['', Validators.required],
      description: ['', Validators.required],
      geolocation: ['', Validators.required]
    });

  }

  get f() { return this.registerForm.controls; }

  ngOnInit() {
  }

  registerCompany() {
    
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.companyService.registerCompany(this.registerForm.value)
      .pipe(first())
      .subscribe(
        data => {
          debugger
          this.router.navigate(['/admin/dashboard'], { replaceUrl: true });
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }



}
