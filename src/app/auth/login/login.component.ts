import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.loginForm = this.formBuilder.group({
      name: [''],
      surname: [''],
      username: [''],
      password: ['']
    });

    //redireciona pra home caso esteja logado  
    if (localStorage.getItem('token') != null) {
      this.router.navigate(['/admin/first-entry']);
    }
  }

  get f() { return this.loginForm.controls; }

  ngOnInit() {

  }


  login() {
    this.submitted = true;
    
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/admin/first-entry'], { replaceUrl: true });
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Invalid Username/Password!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  addNewUser() { 
    this.authenticationService.addNewUser(this.loginForm.get('name').value,this.loginForm.get('surname').value,this.loginForm.get('username').value,this.loginForm.get('password').value)
      .subscribe(
        data => {     
          Swal.fire(
            'Good job!',
            'Successfuly Update!',
            'success'
          ).then((result) => {
            this.router.navigate(['/auth/login'], { replaceUrl: true });
          });    
          
        },
        error => {
          Swal.fire({
            title: 'Oops...',
            text: 'Invalid Username/Password!',
            icon: 'error',
            confirmButtonText: 'Close'
          })
        });
  }

  

}
